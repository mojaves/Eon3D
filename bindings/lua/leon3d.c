/**************************************************************************
 * eon3dx_lua.c -- Eon3D eXtension and companion tools                    *
 *              -- Lua 5.1 bindings.                                      *
 * (C) 2014 Francesco Romani <fromani at gmail dot com>                   *
 *                                                                        *
 * This software is provided 'as-is', without any express or implied      *
 * warranty.  In no event will the authors be held liable for any damages *
 * arising from the use of this software.                                 *
 *                                                                        *
 * Permission is granted to anyone to use this software for any purpose,  *
 * including commercial applications, and to alter it and redistribute it *
 * freely, subject to the following restrictions:                         *
 *                                                                        *
 * 1. The origin of this software must not be misrepresented; you must    *
 *    not claim that you wrote the original software. If you use this     *
 *    software in a product, an acknowledgment in the product             *
 *    documentation would be appreciated but is not required.             *
 * 2. Altered source versions must be plainly marked as such, and must    *
 *    not be misrepresented as being the original software.               *
 * 3. This notice may not be removed or altered from any source           *
 *    distribution.                                                       *
 *                                                                        *
 **************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <lua5.1/lua.h>
#include <lua5.1/lauxlib.h>

#include "eon3d.h"
#include "eon3dx.h"
#ifdef ENABLE_READER
#include "eon3dx_reader.h"
#endif
#ifdef ENABLE_CONSOLE
#include "eon3dx_console.h"
#endif

enum {
    STR_BUF_LEN = 128
};

#define PTR_MASK 0xFFFFFFFFFFFFFFFC
/* reserve just the two LSB to be 32-bit safe. Just in case. */

inline static void *ptr_raw(void *ptr)
{
    return (void *)((uint64_t)ptr & (PTR_MASK));
}

inline static uint32_t ptr_tags(void *ptr)
{
    return ((uint64_t)ptr & ~(PTR_MASK));
}

inline static int ptr_is_tagged(void *ptr)
{
    return (ptr_tags(ptr) != 0);
}

inline static void *ptr_tag(void *ptr, uint32_t tag)
{
    return (void *)((uint64_t)ptr | (~(PTR_MASK) | tag));
}

enum {
    MASTER_REF = 0x00,
    SHARED_REF = 0x01
};

static inline int ptr_shared(void *ptr)
{
    return (ptr_tags(ptr) & SHARED_REF);
}

/*************************************************************************/

static int version(lua_State *L)
{
    lua_pushstring(L, VERSION);
    return 1;
}

static int full_version(lua_State *L)
{
    lua_pushstring(L, PACKAGE " v" VERSION);
    return 1;
}


/*************************************************************************/

typedef void (*Finalizer)(void *);

inline static void *push_Generic(lua_State *L, void *p,
                                 uint32_t reftype, const char *typename)
{
    void **pp = lua_newuserdata(L, sizeof(void *));
    *pp = ptr_tag(p, reftype);
    luaL_getmetatable(L, typename);
    lua_setmetatable(L, -2);
    return p;
}

inline static int generic_gc(lua_State *L, const char *typename,
                             Finalizer fini)
{
    void **pp = luaL_checkudata(L, 1, typename);
    void *p = ptr_raw(*pp);
    if (p && !ptr_shared(*pp)) {
        fini(p);
        *pp = NULL;
    }
    return 0;
}

inline static void *check_Generic(lua_State *L, int n, const char *typename)
{
    void **pp = luaL_checkudata(L, n, typename);
    void *p = ptr_raw(*pp);
    if (p == NULL) {
        char errbuf[128] = { '\0' }; /* FIXME */
        snprintf(errbuf, sizeof(errbuf), "%s has been freed", typename);
        luaL_argerror(L, n, errbuf);
    }
    return p;
}

/*************************************************************************/

typedef EON_PointScr *PointScr;


static int pointscrm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.pointscr", (Finalizer)free);
}

static PointScr check_PointScr(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.pointscr");
}

static int pointscr_new(lua_State *L)
{
    EON_PointScr *p = push_Generic(L,
                                  malloc(sizeof(EON_PointScr)),
                                  MASTER_REF,
                                  "eon3d.pointscr");
    p->X = luaL_checkint(L, 1);
    p->Y = luaL_checkint(L, 2);
    p->Z = luaL_checknumber(L, 3);
    return 1;
}

static int pointscrm_tostring(lua_State *L)
{
    EON_PointScr *p = check_PointScr(L,1);
    lua_pushfstring(L, "<eon3d.pointscr@%p x=%i y=%i z=%f>",
                    p, p->X, p->Y, p->Z, p);
    return 1;
}

static int pointscrm__index(lua_State *L)
{
    EON_PointScr *p = check_PointScr(L, 1);
    const char *field = luaL_checkstring(L, 2);

    if (strcmp(field, "x") == 0 || strcmp(field, "X") == 0) {
        lua_pushinteger(L, p->X);
    } else if (strcmp(field, "y") == 0 || strcmp(field, "Y") == 0) {
        lua_pushinteger(L, p->Y);
    } else if (strcmp(field, "z") == 0 || strcmp(field, "Z") == 0) {
        lua_pushnumber(L, p->Z);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}


/*************************************************************************/

typedef EON_Point3D *Point3D;


static EON_Point3D *push_Point3DRef(lua_State *L, Point3D p)
{
    return push_Generic(L, p, SHARED_REF, "eon3d.point3d");
}

static int point3dm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.point3d", (Finalizer)free);
}

static Point3D check_Point3D(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.point3d");
}

static int point3d_new(lua_State *L)
{
    EON_Point3D *p = push_Generic(L,
                                  malloc(sizeof(EON_Point3D)),
                                  MASTER_REF,
                                  "eon3d.point3d");
    p->X = luaL_checknumber(L, 1);
    p->Y = luaL_checknumber(L, 2);
    p->Z = luaL_checknumber(L, 3);
    return 1;
}

static const char *point3dm_str(EON_Point3D *p, const char *name,
                               char *buf, size_t len)
{
    snprintf(buf, len,
             "<%s@%p x=%f y=%f z=%f>",
             name, p, p->X, p->Y, p->Z);
    return buf;
}

static int point3dm_tostring(lua_State *L)
{
    char buf[STR_BUF_LEN] = { '\0' };
    EON_Point3D *p = check_Point3D(L, 1);
    point3dm_str(p, "eon3d.point", buf, sizeof(buf));
    lua_pushfstring(L, "%s", buf);
    return 1;
}

static int point3dm__index(lua_State *L)
{
    EON_Point3D *p = check_Point3D(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "x") == 0 || strcmp(field, "X") == 0) {
        lua_pushnumber(L, p->X);
    } else if (strcmp(field, "y") == 0 || strcmp(field, "Y") == 0) {
        lua_pushnumber(L, p->Y);
    } else if (strcmp(field, "z") == 0 || strcmp(field, "Z") == 0) {
        lua_pushnumber(L, p->Z);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int point3dm__newindex(lua_State *L)
{
    EON_Point3D *p = check_Point3D(L, 1);
    const char *field = luaL_checkstring(L, 2);
    EON_Float value = luaL_checknumber(L, 3);
    if (strcmp(field, "x") == 0 || strcmp(field, "X") == 0) {
        p->X = value;
    } else if (strcmp(field, "y") == 0 || strcmp(field, "Y") == 0) {
        p->Y = value;
    } else if (strcmp(field, "z") == 0 || strcmp(field, "Z") == 0) {
        p->Z = value;
    }
    return 0;
}


/*************************************************************************/

typedef EON_Color *Color;


static Color push_Color(lua_State *L, Color c, uint32_t reftype)
{
    return push_Generic(L, c, reftype, "eon3d.color");
}

static int colorm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.color", (Finalizer)free);
}

static Color check_Color(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.color");
}

static int push_ColorRGB(lua_State *L, int R, int G, int B, int A)
{
    EON_Color *c = push_Color(L, malloc(sizeof(EON_Color)), MASTER_REF);
    c->R = R;
    c->G = G;
    c->B = B;
    c->A = A;
    return 1;
}


/* FIXME */
static int color_new(lua_State *L)
{
    int i;
    int args[4];
    args[0] = luaL_checkint(L, 1); /* red */
    args[1] = luaL_checkint(L, 2); /* green */
    args[2] = luaL_checkint(L, 3); /* blue */
    args[3] = luaL_optint(L, 4, 255); /* alpha */

    for (i=0; i<4; i++) {
        luaL_argcheck(L, args[i] >= 0 && args[i] <= 255, i+1,
                      "values must be >= 0 and <= 255");
    }

    return push_ColorRGB(L, args[0], args[1], args[2], args[3]);
}

static const char *colorm_str(EON_Color *c, const char *name,
                              char *buf, size_t len)
{
    snprintf(buf, len,
             "<%s@%p r=%d g=%d b=%d a=%d>",
             name, c, c->R, c->G, c->B, c->A);
    return buf;
}

static int colorm_tostring(lua_State *L)
{
    char buf[STR_BUF_LEN] = { '\0' };
    EON_Color *c = check_Color(L, 1);
    colorm_str(c, "eon3d.color", buf, sizeof(buf));
    lua_pushfstring(L, "%s", buf);
    return 1;
}

static int colorm__index(lua_State *L)
{
    EON_Color *c = check_Color(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "red") == 0 || strcmp(field, "R") == 0) {
        lua_pushinteger(L, c->R);
    } else if (strcmp(field, "green") == 0 || strcmp(field, "G") == 0) {
        lua_pushinteger(L, c->G);
    } else if (strcmp(field, "blue") == 0 || strcmp(field, "B") == 0) {
        lua_pushinteger(L, c->B);
    } else if (strcmp(field, "alpha") == 0 || strcmp(field, "A") == 0) {
        lua_pushinteger(L, c->A);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int colorm__newindex(lua_State *L)
{
    EON_Color *c = check_Color(L, 1);
    const char *field = luaL_checkstring(L, 2);
    int value = luaL_checkint(L, 3);
    luaL_argcheck(L, value >= 0 && value <= 255, 3,
                  "value must be >= 0 and <= 255");
    if (strcmp(field, "red") == 0 || strcmp(field, "R") == 0) {
        c->R = value;
    } else if (strcmp(field, "green") == 0 || strcmp(field, "G") == 0) {
        c->G = value;
    } else if (strcmp(field, "blue") == 0 || strcmp(field, "B") == 0) {
        c->B = value;
    } else if (strcmp(field, "alpha") == 0 || strcmp(field, "A") == 0) {
        c->A = value;
    }
    return 0;
}


/*************************************************************************/

typedef EON_Frame *Frame;


static Frame push_Frame(lua_State *L, Frame fr, uint32_t reftype)
{
    return push_Generic(L, fr, reftype, "eon3d.frame");
}

static Frame check_Frame(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.frame");
}

static int framem_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.frame", (Finalizer)EON_FrameDelete);
}

static int frame_new(lua_State *L)
{
    EON_uInt32 Width = luaL_checkint(L, 1);
    EON_uInt32 Height = luaL_checkint(L, 2);
    EON_uInt32 Bpp = luaL_checkint(L, 3);
    Frame fr = push_Frame(L, EON_FrameCreate(Width, Height, Bpp), MASTER_REF);
    if (fr == NULL) {
        return luaL_error(L, "failed with EON_FrameCreate");
    }
    return 1;
}

static int framem_tostring(lua_State *L)
{
    Frame fr = check_Frame(L, 1);
    if (fr) {
        lua_pushfstring(L, "<eon3d.frame@%p>", fr);
    } else {
        lua_pushfstring(L, "<eon3d.frame@NULL>");
    }
    return 1;
}

static int framem__index(lua_State *L)
{
    Frame fr = check_Frame(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "width") == 0) {
        lua_pushinteger(L, fr->Width);
    } else if (strcmp(field, "height") == 0) {
        lua_pushinteger(L, fr->Height);
    } else if (strcmp(field, "bpp") == 0) {
        lua_pushinteger(L, fr->Bpp);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int framem_size(lua_State *L)
{
    Frame fr = check_Frame(L, 1);
    lua_pushinteger(L, EON_FrameSize(fr));
    return 1;
}

static int framem_clear(lua_State *L)
{
    Frame fr = check_Frame(L, 1);
    EON_FrameClear(fr);
    return 0;
}


/*************************************************************************/

typedef EON_Mat *Material;


static Material push_Material(lua_State *L, Material mat)
{
    return push_Generic(L, mat, MASTER_REF, "eon3d.material");
}

static Material check_Material(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.material");
}

static int materialm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.material", (Finalizer)EON_MatDelete);
}

static int material_new(lua_State *L)
{
    Material mat = push_Material(L, EON_MatCreate());
    if (mat == NULL) {
        return luaL_error(L, "failed with EON_MatCreate");
    }
    return 1;
}

static int materialm_tostring(lua_State *L)
{
    Material *matp = luaL_checkudata(L, 1, "eon3d.material");
    Material mat = *matp;
    if (mat) {
        char amb[STR_BUF_LEN] = { '\0' };
        char diff[STR_BUF_LEN] = { '\0' };
        char spec[STR_BUF_LEN] = { '\0' };
        lua_pushfstring(L,
                        "<eon3d.material@%p %s %s %s shininess=%d fade=%f texture=%s>",
                        mat,
                        colorm_str(&mat->Ambient, "ambient", amb, sizeof(amb)),
                        colorm_str(&mat->Diffuse, "diffuse", diff, sizeof(diff)),
                        colorm_str(&mat->Specular, "specular", spec, sizeof(spec)),
                        mat->Shininess, mat->FadeDist, (mat->Texture) ?"yes" :"no");
    } else {
        lua_pushfstring(L, "<eon3d.material@NULL>");
    }
    return 1;
}

static int materialm__index(lua_State *L)
{
    Material mat = check_Material(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "ambient") == 0) {
        push_Color(L, &(mat->Ambient), SHARED_REF);
    } else if (strcmp(field, "diffuse") == 0) {
        push_Color(L, &(mat->Diffuse), SHARED_REF);
    } else if (strcmp(field, "specular") == 0) {
        push_Color(L, &(mat->Specular), SHARED_REF);
    } else if (strcmp(field, "shininess") == 0) {
        lua_pushinteger(L, mat->Shininess);
    } else if (strcmp(field, "fadedist") == 0) {
        lua_pushnumber(L, mat->FadeDist);
    } else if (strcmp(field, "shadetype") == 0) { /* FIXME */
        lua_pushinteger(L, mat->ShadeType);
    } else if (strcmp(field, "perspectivecorrect") == 0) {
        lua_pushinteger(L, mat->PerspectiveCorrect);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int materialm__newindex(lua_State *L)
{
    Material mat = check_Material(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "shadetype") == 0) {
        EON_sInt val = luaL_checkint(L, 3); /* FIXME */
        mat->ShadeType = val;
    }
    return 0;
}

static int materialm_init(lua_State *L)
{
    Material mat = check_Material(L, 1);
    EON_MatInit(mat);
    return 0;
}


/*************************************************************************/

typedef EON_Light *Light;


static Light push_Light(lua_State *L, Light l)
{
    return push_Generic(L, l, MASTER_REF, "eon3d.light");
}

static Light check_Light(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.light");
}

static int lightm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.light", (Finalizer)EON_LightDelete);
}

static int light_new(lua_State *L)
{
    EON_sInt mode = luaL_checkint(L, 1); /* FIXME */
    EON_Float X = luaL_checknumber(L, 2);
    EON_Float Y = luaL_checknumber(L, 3);
    EON_Float Z = luaL_checknumber(L, 4);
    EON_Float intensity = luaL_checknumber(L, 5);
    EON_Float halfdist = luaL_checknumber(L, 6);
    Light l = push_Light(L, EON_LightNew(mode, X, Y, Z, intensity, halfdist));
    if (l == NULL) {
        return luaL_error(L, "failed with EON_LightNew");
    }
    return 1;
}

static int lightm_tostring(lua_State *L)
{
    Light *lp = luaL_checkudata(L, 1, "eon3d.light");
    Light l = *lp;
    if (l) {
        char buf[STR_BUF_LEN] = { '\0' };
        point3dm_str(&l->Pos, "position", buf, sizeof(buf));
        lua_pushfstring(L,
                        "<eon3d.light@%p %s intensity=%f>",
                        buf, l, l->Intensity);
    } else {
        lua_pushfstring(L, "<eon3d.light@NULL>");
    }
    return 1;
}

static int lightm__index(lua_State *L)
{
    Light l = check_Light(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "position") == 0) {
        push_Point3DRef(L, &(l->Pos));
    } else if (strcmp(field, "intensiy") == 0) {
        lua_pushnumber(L, l->Intensity);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int lightm__newindex(lua_State *L)
{
    Light l = check_Light(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "position") == 0) {
        EON_Point3D *val = check_Point3D(L, 3);
        EON_LightMove(l, val->X, val->Y, val->Z);
    } else if (strcmp(field, "intensity") == 0) {
        EON_Float val = luaL_checknumber(L, 3);
        l->Intensity = val; /* yup, that's cheating. */
    }
    return 0;
}

static int lightm_move(lua_State *L)
{
    Light l = check_Light(L, 1);
    EON_Float X = luaL_checknumber(L, 2);
    EON_Float Y = luaL_checknumber(L, 3);
    EON_Float Z = luaL_checknumber(L, 4);
    EON_LightMove(l, X, Y, Z);
    return 0;
}


/*************************************************************************/

typedef EON_Mesh *Mesh;


static Mesh push_Mesh(lua_State *L, Mesh m)
{
    return push_Generic(L, m, MASTER_REF, "eon3d.mesh");
}

static Mesh check_Mesh(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.mesh");
}

static int meshm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.mesh", (Finalizer)EON_MeshDelete);
}

static int mesh_new(lua_State *L)
{
    EON_uInt32 Vertices = luaL_checkint(L, 1);
    EON_uInt32 Faces = luaL_checkint(L, 2);
    Mesh m = push_Mesh(L, EON_MeshCreate(Vertices, Faces));
    if (m == NULL) {
        return luaL_error(L, "failed with EON_MeshCreate");
    }
    return 1;
}

static int mesh_clone(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    Mesh m2 = push_Mesh(L, EON_MeshClone(m));
    if (m2 == NULL) {
        return luaL_error(L, "failed with EON_MeshClone");
    }
    return 1;
}

#ifdef ENABLE_READER
static int mesh_readply(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    Material mat = check_Material(L, 2);
    Mesh m = push_Mesh(L, EONx_ReadPLYMesh(filename, mat));
    if (m == NULL) {
        return luaL_error(L, "failed with EON_ReadPLYMesh");
    }
    return 1;
}
#endif /* ENABLE_READER */

static int meshm_tostring(lua_State *L)
{
    Mesh *mp = luaL_checkudata(L, 1, "eon3d.mesh");
    Mesh m = *mp;
    if (m) {
        char pos[STR_BUF_LEN] = { '\0' };
        char rot[STR_BUF_LEN] = { '\0' };
        point3dm_str(&m->Position, "position", pos, sizeof(pos));
        point3dm_str(&m->Rotation, "rotation", rot, sizeof(rot));
        lua_pushfstring(L,
                        "<eon3d.mesh@%p %s %s vertices=%d faces=%d>",
                        m, pos, rot, m->NumVertices, m->NumFaces);
    } else {
        lua_pushfstring(L, "<eon3d.mesh@NULL>");
    }
    return 1;
}

static int meshm__index(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "rotation") == 0) {
        push_Point3DRef(L, &(m->Rotation)); 
    } else if (strcmp(field, "position") == 0) {
        push_Point3DRef(L, &(m->Position)); 
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int meshm_scale(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    EON_MeshScale(m, luaL_checknumber(L, 2));
    return 0;
}

static int meshm_stretch(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    EON_Float X = luaL_checknumber(L, 2);
    EON_Float Y = luaL_checknumber(L, 3);
    EON_Float Z = luaL_checknumber(L, 4);
    EON_MeshStretch(m, X, Y, Z);
    return 0;
}

/* TODO
void EON_MeshCentroid(const EON_Mesh *o,
                     EON_Float *x, EON_Float *y, EON_Float *z);
*/

static int meshm_center(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    EON_MeshCenter(m);
    return 0;
}

/* TODO
void EON_MeshMeasure(const EON_Mesh *o,
                    EON_Float *dx, EON_Float *dy, EON_Float *dz);
*/
static int meshm_translate(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    EON_Float X = luaL_checknumber(L, 2);
    EON_Float Y = luaL_checknumber(L, 3);
    EON_Float Z = luaL_checknumber(L, 4);
    EON_MeshTranslate(m, X, Y, Z);
    return 0;
}

static int meshm_flipnormals(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    EON_MeshFlipNormals(m);
    return 0;
}

static int meshm_calcnormals(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    EON_MeshCalcNormals(m);
    return 0;
}

/* TODO
void EON_MeshSetMat(EON_Mesh *o, EON_Mat *m, EON_Bool th);
*/

static int new_Mesh(lua_State *L, Mesh m, const char *builder)
{
    Mesh m2 = push_Mesh(L, m);
    if (m2 == NULL) {
        return luaL_error(L, "failed with %s", builder);
    }
    return 1;
}

static int meshm_boundingbox(lua_State *L)
{
    Mesh m = check_Mesh(L, 1);
    Material mat = check_Material(L, 2);
    return new_Mesh(L, EON_MeshBoundingBox(m, mat),
                    "EON_MeshBoundingBox");
}

static int mesh_plane(lua_State *L)
{
    EON_Float w = luaL_checknumber(L, 1);
    EON_Float d = luaL_checknumber(L, 2);
    EON_uInt res = luaL_checkint(L, 3);
    Material mat = check_Material(L, 4);
    return new_Mesh(L, EON_MakePlane(w, d, res, mat),
                    "EON_MakePlane");
}

static int mesh_box(lua_State *L)
{
    EON_Float w = luaL_checknumber(L, 1);
    EON_Float d = luaL_checknumber(L, 2);
    EON_Float h = luaL_checknumber(L, 3);
    Material mat = check_Material(L, 4);
    return new_Mesh(L, EON_MakeBox(w, d, h, mat),
                    "EON_MakeBox");
}

static int mesh_cone(lua_State *L)
{
    EON_Float r = luaL_checknumber(L, 1);
    EON_Float h = luaL_checknumber(L, 2);
    EON_uInt div = luaL_checkint(L, 3);
    EON_Bool cap = luaL_checkint(L, 4);
    Material mat = check_Material(L, 5);
    return new_Mesh(L, EON_MakeCone(r, h, div, cap, mat),
                    "EON_MakeCone");
}

static int mesh_cylinder(lua_State *L)
{
    EON_Float r = luaL_checknumber(L, 1);
    EON_Float h = luaL_checknumber(L, 2);
    EON_uInt divr = luaL_checkint(L, 3);
    EON_Bool capt = luaL_checkint(L, 4);
    EON_Bool capb = luaL_checkint(L, 5);
    Material mat = check_Material(L, 6);
    return new_Mesh(L, EON_MakeCylinder(r, h, divr, capt, capb, mat),
                    "EON_MakeCylinder");
}

static int mesh_sphere(lua_State *L)
{
    EON_Float r = luaL_checknumber(L, 1);
    EON_uInt divr = luaL_checkint(L, 2);
    EON_uInt divh = luaL_checkint(L, 3);
    Material mat = check_Material(L, 4);
    return new_Mesh(L, EON_MakeSphere(r, divr, divh, mat),
                    "EON_MakeSphere");
}

static int mesh_torus(lua_State *L)
{
    EON_Float r1 = luaL_checknumber(L, 1);
    EON_Float r2 = luaL_checknumber(L, 2);
    EON_uInt divrot = luaL_checkint(L, 3);
    EON_uInt divrad = luaL_checkint(L, 4);
    Material mat = check_Material(L, 5);
    return new_Mesh(L, EON_MakeTorus(r1, r2, divrot, divrad, mat),
                    "EON_MakeTorus");
}

/*************************************************************************/

typedef EON_Cam *Camera;


static Camera push_Camera(lua_State *L, Camera cam, uint32_t reftype)
{
    return push_Generic(L, cam, reftype, "eon3d.camera");
}

static Camera check_Camera(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.camera");
}

static int cameram_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.camera", (Finalizer)EON_CamDelete);
}

static int camera_new(lua_State *L)
{
    EON_uInt Width = luaL_checkint(L, 1);
    EON_uInt Height = luaL_checkint(L, 2);
    EON_Float AspectRatio = luaL_checknumber(L, 3);
    EON_Float FieldOfView = luaL_checknumber(L, 4);
    EON_Cam *c = EON_CamCreate(Width, Height, AspectRatio, FieldOfView);
    Camera cam = push_Camera(L, c, MASTER_REF);
    if (cam == NULL) {
        return luaL_error(L, "failed with EON_CamCreate");
    }
    return 1;
}

static int cameram_tostring(lua_State *L)
{
    Camera *camp = luaL_checkudata(L, 1, "eon3d.camera");
    Camera cam = *camp;
    if (cam) {
        char pos[STR_BUF_LEN] = { '\0' };
        point3dm_str(&cam->Pos, "target", pos, sizeof(pos));
        lua_pushfstring(L,
                        "<eon3d.camera@%p screen=%ix%i ar=%f fov=%f %s>",
                        cam, cam->ScreenWidth, cam->ScreenHeight, cam->AspectRatio, cam->Fov,
                        pos);
    } else {
        lua_pushfstring(L, "<eon3d.camera@NULL>");
    }
    return 1;
}

/* TODO
    EON_uInt16 ScreenWidth, ScreenHeight;
    EON_sInt CenterX, CenterY;
*/

static int cameram__index(lua_State *L)
{
    Camera cam = check_Camera(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "position") == 0) {
        push_Point3DRef(L, &(cam->Pos));
    } else if (strcmp(field, "fieldofview") == 0) {
        lua_pushnumber(L, cam->Fov);
    } else if (strcmp(field, "aspectratio") == 0) {
        lua_pushnumber(L, cam->AspectRatio);
    } else if (strcmp(field, "pitch") == 0) {
        lua_pushnumber(L, cam->Pitch);
    } else if (strcmp(field, "pan") == 0) {
        lua_pushnumber(L, cam->Pan);
    } else if (strcmp(field, "roll") == 0) {
        lua_pushnumber(L, cam->Roll);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}


static int cameram_target(lua_State *L)
{
    Camera cam = check_Camera(L, 1);
    EON_Float X = luaL_checknumber(L, 2);
    EON_Float Y = luaL_checknumber(L, 3);
    EON_Float Z = luaL_checknumber(L, 4);
    EON_CamSetTarget(cam, X, Y, Z);
    return 0;
}


/*************************************************************************/

typedef EON_Rend *Renderer;


static Renderer check_Renderer(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.renderer");
}

static int rendererm_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.renderer", (Finalizer)EON_RendDelete);
}

static int renderer_new(lua_State *L)
{
    Camera cam = check_Camera(L, 1);
    Renderer rend = push_Generic(L,
                                 EON_RendCreate(cam),
                                 MASTER_REF,
                                 "eon3d.renderer");
    if (rend == NULL) {
        return luaL_error(L, "failed with EON_RendCreate");
    }
    return 1;
}

static int rendererm_tostring(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    if (rend) {
        lua_pushfstring(L, "<eon3d.renderer@%p>", rend);
    } else {
        lua_pushfstring(L, "<eon3d.renderer@NULL>");
    }
    return 1;
}

static int rendererm_renderbegin(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    EON_RenderBegin(rend);
    return 0;
}

static int rendererm_renderlight(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    Light light = check_Light(L, 2);
    EON_RenderLight(rend, light);
    return 0;
}

static int rendererm_rendermesh(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    Mesh mesh = check_Mesh(L, 2);
    EON_RenderMesh(rend, mesh);
    return 0;
}

static int rendererm_renderend(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    Frame frame = check_Frame(L, 2);
    EON_RenderEnd(rend, frame);
    return 0;
}

static int foreach(lua_State *L, int t,
                   int (*visitor)(lua_State *L, void *ud),
                   void *ud)
{
    int num = 0;
    /* table is in the stack at index 't' */
    lua_pushnil(L);  /* first key */
    while (lua_next(L, t) != 0) {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
        visitor(L, ud);
        /* removes 'value'; keeps 'key' for next iteration */
        lua_pop(L, 1);
        num++;
    }
    lua_pop(L, 1); /* removes the last value */
    return num;
}

static int visit_lights(lua_State *L, void *ud)
{
    Renderer rend = ud;
    Light light = check_Light(L, -1);
    EON_RenderLight(rend, light);
    return 0;
}

static int visit_meshes(lua_State *L, void *ud)
{
    Renderer rend = ud;
    Mesh mesh = check_Mesh(L, -1);
    EON_RenderMesh(rend, mesh);
    return 0;
}

static int rendererm_rendermeshes(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    if (!lua_istable(L, 2)) {
        return luaL_argerror(L, 2, "not a table");
    }
    foreach(L, -1, visit_meshes, rend);
    return 0;
}

static int rendererm_rendescene(lua_State *L)
{
    Renderer rend = check_Renderer(L, 1);
    Frame frame = check_Frame(L, 3);
    if (!lua_istable(L, 2)) {
        return luaL_argerror(L, 2, "not a table");
    }
    
    EON_RenderBegin(rend);

    lua_settop(L, 2);

    lua_pushstring(L, "lights");
    lua_gettable(L, -2);
    foreach(L, -1, visit_lights, rend);
    lua_pop(L, 1);

    lua_pushstring(L, "meshes");
    lua_gettable(L, -2);
    foreach(L, -1, visit_meshes, rend);
    lua_pop(L, 1);

    lua_pop(L, 1);

    EON_RenderEnd(rend, frame);
    return 0;
}

/*************************************************************************/
#ifdef ENABLE_CONSOLE

typedef EONx_Console *Console;


static Console check_Console(lua_State *L, int n)
{
    return check_Generic(L, n, "eon3d.console");
}

static int consolem_gc(lua_State *L)
{
    return generic_gc(L, "eon3d.console", (Finalizer)EONx_ConsoleDelete);
}

static int console_new(lua_State *L)
{
    EON_uInt sw = luaL_checkint(L, 1);
    EON_uInt sh = luaL_checkint(L, 2);
    EON_Float fov = luaL_checknumber(L, 3);
    Console con = push_Generic(L,
                               EONx_ConsoleCreate(sw, sh, fov),
                               MASTER_REF, "eon3d.console");
    if (con == NULL) {
        return luaL_error(L, "failed with EON_ConsoleCreate");
    }
    return 1;
}

static int console_startup(lua_State *L)
{
    const char *title = luaL_checkstring(L, 1);
    const char *icon = luaL_optstring(L, 2, "");
    int ret = EONx_ConsoleStartup(title, icon);
    lua_pushinteger(L, ret);
    return 1;
}

static int console_shutdown(lua_State *L)
{
    int ret = EONx_ConsoleShutdown();
    lua_pushinteger(L, ret);
    return 1;
}

static int consolem_tostring(lua_State *L)
{
    Console con = check_Console(L, 1);
    if (con) {
        lua_pushfstring(L, "<eon3d.console@%p>", con);
    } else {
        lua_pushfstring(L, "<eon3d.console@NULL>");
    }
    return 1;
}

static int consolem__index(lua_State *L)
{
    Console con = check_Console(L, 1);
    const char *field = luaL_checkstring(L, 2);
    if (strcmp(field, "frame") == 0) {
        push_Frame(L, EONx_ConsoleGetFrame(con), SHARED_REF);
    } else if (strcmp(field, "camera") == 0) {
        push_Camera(L, EONx_ConsoleGetCamera(con), SHARED_REF);
    } else {
        lua_getmetatable(L, 1);
        lua_pushvalue(L, 2);
        lua_gettable(L, -2);
    }
    return 1;
}

static int consolem_nextevent(lua_State *L)
{
    Console con = check_Console(L, 1);
    int ret = EONx_ConsoleNextEvent(con);
    lua_pushinteger(L, ret);
    return 1;
}

static int consolem_clearframe(lua_State *L)
{
    Console con = check_Console(L, 1);
    int ret = EONx_ConsoleClearFrame(con);
    lua_pushinteger(L, ret);
    return 1;
}

static int consolem_showframe(lua_State *L)
{
    Console con = check_Console(L, 1);
    int ret = EONx_ConsoleShowFrame(con);
    lua_pushinteger(L, ret);
    return 1;
}

#endif /* ENABLE_CONSOLE */
/*************************************************************************/

static const struct luaL_Reg pointscr_f[] = {
    { "new", pointscr_new },
    { NULL, NULL }
};

static const struct luaL_Reg pointscr_m[] = {
    { "__gc",       pointscrm_gc },
    { "__tostring", pointscrm_tostring },
    { "__index",    pointscrm__index },
    { NULL, NULL }
};


static const struct luaL_Reg point3d_f[] = {
    { "new", point3d_new },
    { NULL, NULL }
};

static const struct luaL_Reg point3d_m[] = {
    { "__gc",       point3dm_gc },
    { "__tostring", point3dm_tostring },
    { "__index",    point3dm__index },
    { "__newindex", point3dm__newindex },
    { NULL, NULL }
};


static const struct luaL_Reg color_f[] = {
    { "new", color_new },
    { NULL, NULL }
};

static const struct luaL_Reg color_m[] = {
    { "__gc",       colorm_gc },
    { "__tostring", colorm_tostring },
    { "__index",    colorm__index },
    { "__newindex", colorm__newindex },
    { NULL, NULL }
};


static const struct luaL_Reg frame_f[] = {
    { "new", frame_new},
    { NULL, NULL}
};

static const struct luaL_Reg frame_m[] = {
    { "__gc",       framem_gc },
    { "__tostring", framem_tostring },
    { "__index",    framem__index},
    { "size",       framem_size },
    { "clear",      framem_clear },
    { NULL, NULL }
};


static const struct luaL_Reg material_f[] = {
    { "new", material_new},
    { NULL, NULL}
};

static const struct luaL_Reg material_m[] = {
    { "__gc",       materialm_gc },
    { "__tostring", materialm_tostring },
    { "__index",    materialm__index},
    { "__newindex", materialm__newindex},
    { "init",       materialm_init },
    { NULL, NULL }
};


static const struct luaL_Reg light_f[] = {
    { "new", light_new},
    { NULL, NULL}
};

static const struct luaL_Reg light_m[] = {
    { "__gc",       lightm_gc },
    { "__tostring", lightm_tostring },
    { "__index",    lightm__index},
    { "__newindex", lightm__newindex},
    { "move",       lightm_move },
    { NULL, NULL }
};


static const struct luaL_Reg mesh_f[] = {
    { "new",        mesh_new },
    { "clone",      mesh_clone },
#ifdef ENABLE_READER
    { "readply",    mesh_readply },
#endif /* ENABLE_READER */
    { "plane",      mesh_plane },
    { "box",        mesh_box },
    { "cone",       mesh_cone },
    { "cylinder",   mesh_cylinder },
    { "sphere",     mesh_sphere },
    { "torus",      mesh_torus },
    { NULL, NULL}
};

static const struct luaL_Reg mesh_m[] = {
    { "__gc",       meshm_gc },
    { "__tostring", meshm_tostring },
    { "__index",    meshm__index},
    { "scale",      meshm_scale },
    { "stretch",    meshm_stretch },
    { "center",     meshm_center },
    { "translate",  meshm_translate },
    { "flipnormals",meshm_flipnormals },
    { "calcnormals",meshm_calcnormals },
    { "boundingbox",meshm_boundingbox },
    { NULL, NULL }
};


static const struct luaL_Reg camera_f[] = {
    { "new", camera_new},
    { NULL, NULL}
};

static const struct luaL_Reg camera_m[] = {
    { "__gc",       cameram_gc },
    { "__tostring", cameram_tostring },
    { "__index",    cameram__index},
    { "target",     cameram_target },
    { NULL, NULL }
};


static const struct luaL_Reg renderer_f[] = {
    { "new", renderer_new},
    { NULL, NULL}
};

static const struct luaL_Reg renderer_m[] = {
    { "__gc",           rendererm_gc },
    { "__tostring",     rendererm_tostring },
    { "renderbegin",    rendererm_renderbegin },
    { "renderlight",    rendererm_renderlight },
    { "rendermesh",     rendererm_rendermesh },
    { "renderend",      rendererm_renderend },
/*
    { "rendermeshes",   rendererm_rendermeshes },
    { "renderscene",    rendererm_renderscene },
*/    
    { NULL, NULL }
};

#ifdef ENABLE_CONSOLE
static const struct luaL_Reg console_f[] = {
    { "new",      console_new },
    { "startup",  console_startup },
    { "shutdown", console_shutdown },
    { NULL, NULL}
};

static const struct luaL_Reg console_m[] = {
    { "__gc",       consolem_gc },
    { "__tostring", consolem_tostring },
    { "__index",    consolem__index },
    { "nextevent",  consolem_nextevent },
    { "clearframe", consolem_clearframe },
    { "showframe",  consolem_showframe },
    { NULL, NULL }
};
#endif /* ENABLE_CONSOLE */


static const struct luaL_Reg eon3d_funcs[] = {
    { "version",        version },
    { "full_version",   full_version },
    { NULL, NULL }
};


/*************************************************************************/

static void register_type(lua_State *L, const char *name,
                          const struct luaL_Reg *meths,
                          const struct luaL_Reg *funcs)
{
    luaL_newmetatable(L, name);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_register(L, NULL, meths);
    luaL_register(L, name, funcs);
    return;
}

int luaopen_leon3d(lua_State *L)
{
    register_type(L, "eon3d.pointscr", pointscr_m, pointscr_f);
    register_type(L, "eon3d.point3d",  point3d_m,  point3d_f);
    register_type(L, "eon3d.color",    color_m,    color_f);
    register_type(L, "eon3d.frame",    frame_m,    frame_f);
    register_type(L, "eon3d.material", material_m, material_f);
    register_type(L, "eon3d.light",    light_m,    light_f);
    register_type(L, "eon3d.mesh",     mesh_m,     mesh_f);
    register_type(L, "eon3d.camera",   camera_m,   camera_f);
    register_type(L, "eon3d.renderer", renderer_m, renderer_f);
#ifdef ENABLE_CONSOLE
    register_type(L, "eon3d.console",  console_m,  console_f);
#endif /* ENABLE_CONSOLE */

    luaL_register(L, "eon3d", eon3d_funcs);

    return 1;
}

/* vim: set ts=4 sw=4 et */
/* EOF */

