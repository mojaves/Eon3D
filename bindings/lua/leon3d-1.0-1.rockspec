package = "LEon3D"
version = "1.0-1"
source = {
   url = "git://github.com/mojaves/Eon3D"
-- tag
}
description = {
   summary = "Eon3D bindings",
   detailed = [[
      Eon3D is a simplistic, minimal, self contained
      3D software renderer.
      Those is the Lua bindings.
   ]],
   homepage = "https://github.com/mojaves/Eon3D",
   license = "MIT/X11"
}
dependencies = {
   "lua ~> 5.1"
}
build = {
   type = "builtin",
   modules = {
      leon3d = {
         sources = {"leon3d.c","../../core/eon3d.c","../../core/eon3dx.c"},
         defines = {"EON_INLINE=inline", "EON_PRIVATE=static"},
         incdirs = {"../../core","../../deps/cxkit","../../deps/cxkit/kits"}
      }
   }
}
