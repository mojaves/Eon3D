#!/usr/bin/env python

import sys

def c2bin(fn, out):
    with open(fn, 'rt') as src:
        data = bytearray()
        inside = False
        for line in src:
            if '};\n' == line:
                inside = False
            elif '_DATA[]' in line:
                inside = True
            elif inside:
                row = line.strip()[:-1].split(',')
                for b in row:
                    data.append(chr(int(b, base=16)))
        out.write(data)

for arg in sys.argv[1:]:
    c2bin(arg, sys.stdout)

