/* simple Eon3D benchmark using the PLY model loader. */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <eon3d.h>
#include <eon3dx_reader.h>

#include <logkit.h>



//#define EXE "lsmodel"
#define EXE "exe"


static void usage(void)
{
    fprintf(stderr, "Usage: %s [options] model\n", EXE);
    fprintf(stderr, "    -v verbosity      Verbosity mode.\n");
    fprintf(stderr, "\n");
}


int main(int argc, char *argv[])
{
    const char *filename;
    EON_Mat *material = NULL;
    EON_Mesh *model = NULL;
    EON_Point3D centroid;
    EON_Point3D dimensions;
    CX_LogContext *logger = CX_log_open_console(CX_LOG_MARK, stderr);
    int verbose = 1;
    int ch = -1;

    while (1) {
        ch = getopt(argc, argv, "v:h?");
        if (ch == -1) {
            break;
        }

        switch (ch) {
          case 'v':
            verbose = atoi(optarg);
            break;
          case '?': /* fallthrough */
          case 'h': /* fallthrough */
          default:
            usage();
            return 0;
        }
    }

    /* XXX: watch out here */
    argc -= optind;
    argv += optind;

    if (argc != 1) {
        usage();
        return 1;
    }
    filename = argv[0];

    if (verbose) {
        CX_log_trace(logger, CX_LOG_INFO, EXE,
                     "inspecting model = %s",
                     filename);
    }

    material = EON_MatCreate();

    model = EONx_ReadPLYMesh(filename, material);
    if (model == NULL) {
        CX_log_trace(logger, CX_LOG_ERROR, EXE,
                     "failed to load model '%s', aborting",
                     filename);
        return 1;
    }

    EON_MeshInfo(model, logger);
    EON_MeshCentroid(model, &centroid.X, &centroid.Y, &centroid.Z);

    CX_log_trace(logger, CX_LOG_INFO, EXE,
                 "Centroid at (%f,%f,%f) (X,Y,Z)",
                 centroid.X, centroid.Y, centroid.Z);

    EON_MeshMeasure(model, &dimensions.X, &dimensions.Y, &dimensions.Z);
    CX_log_trace(logger, CX_LOG_INFO, EXE,
                 "Dimensions: dX=%f dY=%f dZ=%f",
                 dimensions.X, dimensions.Y, dimensions.Z);

    return CX_log_close(logger);
}

