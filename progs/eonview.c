/*
 * eonview.c -- 3D model inspector/viewer using Eon3.
 * (C)2014 Francesco Romani <fromani at gmail dot com>
 */

#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include <time.h>

#include <eon3d.h>
#include <eon3dx.h>
#include <eon3dx_console.h>
#include <eon3dx_reader.h>

#include <logkit.h>

typedef struct _Options {
    const char *filename;
    double distance;
    int verbose;
    int frames;
    int shade;
    int width, height;
    double rx, ry, rz;
    int bounding_box;
    int showing_help;
} Options;

typedef struct _EONViewer {
    Options opts;
    CX_LogContext *logger;
    EON_Cam *camera;
    EON_Mesh *model;
    EON_Mesh *box;
} EONViewer;

typedef struct _EnumDesc {
    int value;
    const char *desc;
} EnumDesc;

static const EnumDesc ShadeModes[] = {
    { EON_SHADE_NONE,             "None" },
    { EON_SHADE_FLAT,             "Flat" },
    { EON_SHADE_FLAT_DISTANCE,    "FlatDistance" },
    { EON_SHADE_GOURAUD,          "Gouraud" },
    { EON_SHADE_GOURAUD_DISTANCE, "GouraudDistance" },
    { EON_SHADE_WIREFRAME,        "Wireframe" },
    { 0,                          NULL }
};

static const char *shade_ModeToStr(int shade)
{
    int i = 0;
    int found = 0;
    const char *desc = NULL;
    for (i = 0; !found && ShadeModes[i].desc; i++) {
        if (shade == ShadeModes[i].value) {
            desc = ShadeModes[i].desc;
            found = 1;
        }
    }
    return (found) ?desc :"unknown";
}

static int shade_StrToMode(const char *str)
{
    int i = 0;
    int found = 0;
    int mode = 0;
    for (i = 0; !found && ShadeModes[i].desc; i++) {
        if (!strcasecmp(str, ShadeModes[i].desc)) {
            mode = ShadeModes[i].value;
            found = 1;
        }
    }

    return (found) ?mode :EON_SHADE_NONE;
}

#define EXE "eonview"

static void usage(void)
{
    fprintf(stderr, "Usage: %s [options] model\n", EXE);
    fprintf(stderr, "    -B                Draw the bounding box of the model.\n");
    fprintf(stderr, "    -d dist           Render at the `dist' view distance. Affects performance.\n");
    fprintf(stderr, "    -v verbosity      Verbosity mode.\n");
    fprintf(stderr, "    -n frames         Renders `frames' frames.\n");
    fprintf(stderr, "    -g w,h            Frame size Width,Height in pixel.\n");
    fprintf(stderr, "    -R x,y,z          Rotates around axes of the specified degrees.\n");
    fprintf(stderr, "    -S m              Shading mode.\n");
    fprintf(stderr, "\n");
}

static void opts_Defaults(Options *opts)
{
    memset(opts, 0, sizeof(*opts));
    opts->filename = NULL;
    opts->distance = 50.0;
    opts->verbose = 1;
    opts->frames = -1;
    opts->shade = EON_SHADE_FLAT;
    opts->width = 800;
    opts->height = 600;
    opts->rx = 0.0;
    opts->ry = 0.0;
    opts->rz = 0.0;
    opts->bounding_box = 0;
    opts->showing_help = 0;
    return;
}

static const char *help_text(void)
{
    return \
           "PAGEUP/PAGEDOWN: camera zoom\n"
           "b: draw bounding box\n"
           "s: saves screenshot\n"
           "q: quits\n"
           "h: show this help\n";
}

static int opts_Parse(Options *opts, int argc, char *argv[])
{
    int ch = -1;

    while (1) {
        ch = getopt(argc, argv, "Bd:g:n:R:S:v:h?");
        if (ch == -1) {
            break;
        }

        switch (ch) {
          case 'B':
            opts->bounding_box = 1;
            break;
          case 'd':
            opts->distance = atof(optarg);
            break;
          case 'n':
            opts->frames = atoi(optarg);
            break;
          case 'v':
            opts->verbose = atoi(optarg);
            break;
          case 'g':
            if (sscanf(optarg, "%i,%i",
                       &opts->width, &opts->height) != 2
             || (opts->width  < 320 || opts->width > 1920)
             || (opts->height < 200 || opts->height > 1080)) { // XXX
                opts->width = 800;
                opts->height = 600;
            }
            break;
          case 'R':
            if (sscanf(optarg, "%lf,%lf,%lf",
                       &opts->rx, &opts->ry, &opts->rz) != 3) { // XXX
                opts->rx = 1.0;
                opts->ry = 1.0;
                opts->rz = 1.0;
            }
            break;
          case 'S':
            opts->shade = shade_StrToMode(optarg);
            break;
          case '?': /* fallthrough */
          case 'h': /* fallthrough */
          default:
            usage();
            return 1;
        }
    }

    /* XXX: watch out here */
    argc -= optind;
    argv += optind;

    if (argc != 1) {
        usage();
        return -1;
    }
    opts->filename = argv[0];

    return 0;
}

static int opts_ErrCode(int err)
{
    int ret = 0;
    if (err < 0) {
        ret = -err;
    } // else coalesce on zero
    return ret;
}

static int onkey_Screenshot(EONx_KeyCode key, int status,
                            EONx_Console *ctx, void *userdata)
{
    int err = 0;
    if (status == 1) {
        EONViewer *view = userdata;
        char name[1024] = { '\0' }; // XXX
        EONx_ConsoleMakeName(ctx, name, sizeof(name),
                             "screenshot", "png");
        err = EONx_ConsoleSaveFrame(ctx, name, "png");
        CX_log_trace(view->logger, CX_LOG_INFO, EXE,
                     "saving screenshot to [%s] -> %s (%i)\n",
                     name, (err) ?EONx_ConsoleGetError(ctx) :"OK", err);
    }
    return err;
}

static int onkey_Quit(EONx_KeyCode key, int status,
                      EONx_Console *ctx, void *userdata)
{
    if (status == 1) {
        EONViewer *view = userdata;
        CX_log_trace(view->logger, CX_LOG_INFO, EXE, "Bye!");
    }
    return status;
}

static int onkey_Toggle(EONx_KeyCode key, int status,
                        EONx_Console *ctx, void *userdata)
{
    if (status == 1) {
        EONViewer *view = userdata;
        switch (key) {
        case EONx_Kh:
          view->opts.showing_help = !view->opts.showing_help;
          break;
        case EONx_Kb:
          view->opts.bounding_box = !view->opts.bounding_box;
          break;
        default:
          // TODO
          break;
        }
    }
    return 0;
}

static int onkey_Zoom(EONx_KeyCode key, int status,
                      EONx_Console *ctx, void *userdata)
{
    EONViewer *view = userdata;
    EON_Double dz = view->camera->Pos.Z;
    switch (key) {
      case EONx_KPAGEUP:
        dz = +status;
        break;
      case EONx_KPAGEDOWN:
        dz = -status;
        break;
      default:
        // TODO
        break;
    }
    view->camera->Pos.Z += dz;
    return 0;
}

static int onkey_Rotate(EONx_KeyCode key, int status,
                        EONx_Console *ctx, void *userdata)
{
    EONViewer *view = userdata;
    EON_Double dx = view->opts.rx;
    EON_Double dy = view->opts.ry;
    switch (key) {
      case EONx_KUP:
        dx = status ?(dx + status) :0;
        break;
      case EONx_KDOWN:
        dx = status ?(dx - status) :0;
        break;
      case EONx_KLEFT:
        dy = status ?(dy + status) :0;
        break;
      case EONx_KRIGHT:
        dy = status ?(dy - status) :0;
        break;
      default:
        // TODO
        break;
    }
    view->opts.rx = dx;
    view->opts.ry = dy;
    return 0;
}

static void adjustView(EONViewer *view)
{
    view->model->Rotation.X += view->opts.rx;
    view->model->Rotation.Y += view->opts.ry;
    view->box->Rotation.X += view->opts.rx;
    view->box->Rotation.Y += view->opts.ry;
}

int main(int argc, char *argv[])
{
    int err = 0;
    int frames = 0;
    time_t start = 0;
    time_t stop = 0;

    EON_Light *light;
    EON_Mat *material;
    EON_Mat *boxmaterial;
    EON_Rend *rend;
    EON_Frame *frame;
    EONx_Console *console;
    EONViewer view;

    view.logger = CX_log_open_console(CX_LOG_MARK, stderr);

    opts_Defaults(&view.opts);
    err = opts_Parse(&view.opts, argc, argv);
    if (err) {
        return opts_ErrCode(err);
    }

    CX_log_trace(view.logger, CX_LOG_INFO, EXE,
                 "Shading mode: %s",
                 shade_ModeToStr(view.opts.shade));
    CX_log_trace(view.logger, CX_LOG_INFO, EXE,
                 "Frame Size: %ix%i",
                 view.opts.width, view.opts.height);
    CX_log_trace(view.logger, CX_LOG_INFO, EXE,
                 "Model rotation: X=%f Y=%f Z=%f",
                 view.opts.rx, view.opts.ry, view.opts.rz);

    EONx_ConsoleStartup("Eon3D Model Viewer", NULL);

    material = EON_MatCreate();
    material->ShadeType = view.opts.shade;

    material->Ambient.R = 200;
    material->Ambient.G = 200;
    material->Ambient.B = 200;

    EON_MatInit(material);
    EON_MatInfo(material, view.logger);

    boxmaterial = EON_MatCreate();
    boxmaterial->ShadeType = EON_SHADE_WIREFRAME;
    boxmaterial->Ambient.G = 200;

    EON_MatInit(boxmaterial);
    EON_MatInfo(boxmaterial, view.logger);

    console = EONx_ConsoleCreate(view.opts.width, view.opts.height, 90);

    EONx_ConsoleBindEventKey(console, EONx_KPAGEUP, onkey_Zoom, &view);
    EONx_ConsoleBindEventKey(console, EONx_KPAGEDOWN, onkey_Zoom, &view);
    EONx_ConsoleBindEventKey(console, EONx_KUP, onkey_Rotate, &view);
    EONx_ConsoleBindEventKey(console, EONx_KDOWN, onkey_Rotate, &view);
    EONx_ConsoleBindEventKey(console, EONx_KLEFT, onkey_Rotate, &view);
    EONx_ConsoleBindEventKey(console, EONx_KRIGHT, onkey_Rotate, &view);
    EONx_ConsoleBindEventKey(console, EONx_Kb, onkey_Toggle, &view);
    EONx_ConsoleBindEventKey(console, EONx_Kh, onkey_Toggle, &view);
    EONx_ConsoleBindEventKey(console, EONx_Ks, onkey_Screenshot, &view);
    EONx_ConsoleBindEventKey(console, EONx_Kq, onkey_Quit, &view);

    view.model = EONx_ReadPLYMesh(view.opts.filename, material);
    view.model = EON_MeshCenter(view.model);
    EON_MeshInfo(view.model, view.logger);

    view.box = EON_MeshBoundingBox(view.model, boxmaterial);
    EON_MeshInfo(view.box, view.logger);

    frame = EONx_ConsoleGetFrame(console);
    view.camera = EONx_ConsoleGetCamera(console);
    view.camera->Pos.Z = -view.opts.distance;

    light = EON_LightNew(EON_LIGHT_VECTOR, 0.0, 0.0, 0.0, 1.0, 1.0);

    rend = EON_RendCreate(view.camera);

    start = time(NULL);
    while (!EONx_ConsoleNextEvent(console)) {
        adjustView(&view);
        EONx_ConsoleClearFrame(console);
        EON_RenderBegin(rend);
        EON_RenderLight(rend, light);
        EON_RenderMesh(rend, view.model);
        if (view.opts.bounding_box) { // FIXME
            EON_RenderMesh(rend, view.box);
        }
        if (view.opts.showing_help) {
            EON_TextPrintf(view.camera, frame, "%s", help_text());
        }
        EON_RenderEnd(rend, frame);
        EONx_ConsoleShowFrame(console);
        frames++;
    }
    stop = time(NULL);

    CX_log_trace(view.logger, CX_LOG_INFO, EXE,
                 "%i frames in %f seconds: %.3f FPS\n",
                 frames, (double)stop-(double)start,
                 (double)frames/((double)stop-(double)start));

    return EONx_ConsoleShutdown();
}

