/*
 * landgen.c -- 3D world generator/explorer using Eon3.
 * (C)2014 Francesco Romani <fromani at gmail dot com>
 */

#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#include <eon3d.h>
#include <eon3dx.h>
#include <eon3dx_console.h>
#include <eon3dx_reader.h>

#include <logkit.h>

enum {
    LAND_SIZE = 65000, /* Physical size of land */
    LAND_DIV  = 128    /* Number of divisions of the land.
                          Higher number == more polygons */
};

typedef struct _Ship {
    EON_Double speed;
    int showing_hud;
} Ship;

typedef struct _World {
    EON_Mesh *land;
    EON_Mat *landMat;
    EON_Mesh *sky;
    EON_Mat *skyMat;
} World;

typedef struct _Options {
    double distance;
    int verbose;
    int frames;
    int width, height;
    int seed;
} Options;

typedef struct _Metaverse {
    Options opts;
    World world;
    EON_Cam *cam;
    CX_LogContext *logger;
    Ship ship;
} Metaverse;

#define EXE "landgen"

static void usage(void)
{
    fprintf(stderr, "Usage: %s [options]\n", EXE);
    fprintf(stderr, "    -d dist           Render at the `dist' mv distance. Affects performance.\n");
    fprintf(stderr, "    -v verbosity      Verbosity mode.\n");
    fprintf(stderr, "    -n frames         Renders `frames' frames.\n");
    fprintf(stderr, "    -g w,h            Frame size Width,Height in pixel.\n");
    fprintf(stderr, "    -s seed           Generation seed.\n");
    fprintf(stderr, "\n");
}

static void opts_Defaults(Options *opts)
{
    memset(opts, 0, sizeof(*opts));
    opts->distance = 50.0;
    opts->verbose = 1;
    opts->frames = -1;
    opts->width = 800;
    opts->height = 600;
    opts->seed = 0;
    return;
}

static int opts_Parse(Options *opts, int argc, char *argv[])
{
    int ch = -1;

    while (1) {
        ch = getopt(argc, argv, "d:g:n:R:S:v:h?");
        if (ch == -1) {
            break;
        }

        switch (ch) {
          case 'd':
            opts->distance = atof(optarg);
            break;
          case 'n':
            opts->frames = atoi(optarg);
            break;
          case 'v':
            opts->verbose = atoi(optarg);
            break;
          case 'g':
            if (sscanf(optarg, "%i,%i",
                       &opts->width, &opts->height) != 2
             || (opts->width  < 320 || opts->width > 1920)
             || (opts->height < 200 || opts->height > 1080)) { // XXX
                opts->width = 800;
                opts->height = 600;
            }
            break;
          case 's':
            opts->seed = atoi(optarg);
            break;
          case '?': /* fallthrough */
          case 'h': /* fallthrough */
          default:
            usage();
            return 1;
        }
    }

    return 0;
}

static int opts_ErrCode(int err)
{
    int ret = 0;
    if (err < 0) {
        ret = -err;
    } // else coalesce on zero
    return ret;
}


void world_setupLandscape(World *W,
                          CX_LogContext *Logger)
{
    int i;

    W->landMat = EON_MatCreate();
    W->landMat->ShadeType = EON_SHADE_GOURAUD_DISTANCE;
    W->landMat->Shininess = 1;
    W->landMat->Ambient.R = 12;//255;
    W->landMat->Ambient.G = 200;//255;
    W->landMat->Ambient.B = 5;//255;
    W->landMat->Diffuse.R = 127;
    W->landMat->Diffuse.G = 127;
    W->landMat->Diffuse.B = 127;
    W->landMat->Specular.R = 127;
    W->landMat->Specular.G = 127;
    W->landMat->Specular.B = 127;
    W->landMat->FadeDist = 10000.0;
    W->landMat->PerspectiveCorrect = 16;
    EON_MatInit(W->landMat);
    EON_MatInfo(W->landMat, Logger);

    W->skyMat = EON_MatCreate();
    W->skyMat->ShadeType = EON_SHADE_GOURAUD_DISTANCE;
    W->skyMat->Shininess = 1;
    W->skyMat->Ambient.R = 5;//255;
    W->skyMat->Ambient.G = 12;//255;
    W->skyMat->Ambient.B = 60;//255;
    W->skyMat->Diffuse.R = 127;
    W->skyMat->Diffuse.G = 127;
    W->skyMat->Diffuse.B = 127;
    W->skyMat->Specular.R = 127;
    W->skyMat->Specular.G = 127;
    W->skyMat->Specular.B = 127;
    W->skyMat->FadeDist = 10000.0;
    W->skyMat->PerspectiveCorrect = 32;
    EON_MatInit(W->skyMat);
    EON_MatInfo(W->skyMat, Logger);

    // make our root object the land
    W->land = EON_MakePlane(LAND_SIZE, LAND_SIZE, LAND_DIV-1,
                            W->landMat);
    // give it a nice random bumpy effect
    for (i = 0; i < W->land->NumVertices; i ++) {
        W->land->Vertices[i].c.Y += (float) (rand()%1400)-700;
/*        if (i % 2 == 0) {
            W->land->Vertices[i].y += (float) (rand()%400)-200;
        }
        if (i % 4 == 0) {
            W->land->Vertices[i].y += (float) (rand()%400)-200;
        }
        if (i % 6 == 0) {
            W->land->Vertices[i].y += (float) (rand()%400)-200;
        }
        if (i % 8 == 0) {
            W->land->Vertices[i].y += (float) (rand()%400)-200;
        }
*/
    }
    for (i = 1; i < W->land->NumVertices-1; i ++) {
        W->land->Vertices[i].c.Y = (W->land->Vertices[i-1].c.Y
                                  + W->land->Vertices[i  ].c.Y
                                  + W->land->Vertices[i+1].c.Y)
                                  /3;
    }
    // gotta recalculate normals for backface culling to work right
    EON_MeshCalcNormals(W->land);

    // Make our first child the first sky
    W->sky = EON_MakePlane(LAND_SIZE, LAND_SIZE, 1, W->skyMat);
    W->sky->Position.Y = 2000;
    W->sky->BackfaceCull = 0;

    EON_MeshInfo(W->land, Logger);
    EON_MeshInfo(W->sky,  Logger);

    return;
}

void world_cleanLandscape(World *W)
{
    EON_MeshDelete(W->land);
    EON_MeshDelete(W->sky);
    EON_MatDelete(W->landMat);
    EON_MatDelete(W->skyMat);
    return;
}


// FIXME: move to/from SDL
uint64_t eon_gettime_ms(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec * 1000000 + tv.tv_usec);
}  


static int onkey_Screenshot(EONx_KeyCode key, int status,
                            EONx_Console *ctx, void *userdata)
{
    int err = 0;
    if (status == 1) {
        Metaverse *mv = userdata;
        char name[1024] = { '\0' }; // XXX
        EONx_ConsoleMakeName(ctx, name, sizeof(name),
                             "screenshot", "png");
        err = EONx_ConsoleSaveFrame(ctx, name, "png");
        CX_log_trace(mv->logger, CX_LOG_INFO, EXE,
                     "saving screenshot to [%s] -> %s (%i)\n",
                     name, (err) ?EONx_ConsoleGetError(ctx) :"OK", err);
    }
    return err;
}

static int onkey_Quit(EONx_KeyCode key, int status,
                      EONx_Console *ctx, void *userdata)
{
    if (status == 1) {
        Metaverse *mv = userdata;
        CX_log_trace(mv->logger, CX_LOG_INFO, EXE, "Bye!");
    }
    return 1;
}

static int onkey_Rotate(EONx_KeyCode key, int status,
                        EONx_Console *ctx, void *userdata)
{
    Metaverse *mv = userdata;
    EON_Double s = mv->ship.speed;
    EON_Double mult = 10.0;
    switch (key) {
      case EONx_KUP:
        s = status ?(s + status * mult) :0;
        break;
      case EONx_KDOWN:
        s = status ?(s - status * mult) :0;
        break;
      default:
        // TODO
        break;
    }
    mv->ship.speed = s;
    return 0;
}

static int onkey_Toggle(EONx_KeyCode key, int status,
                        EONx_Console *ctx, void *userdata)
{
    if (status == 1) {
        Metaverse *mv = userdata;
        switch (key) {
        case EONx_Kt:
          mv->ship.showing_hud = !mv->ship.showing_hud;
          break;
        default:
          // TODO
          break;
        }
    }
    return 0;
}



static void adjustVerse(Metaverse *mv)
{
    mv->cam->Pos.Z += mv->ship.speed;
    // wraparound
    if (mv->cam->Pos.X >  LAND_SIZE/2) mv->cam->Pos.X = -LAND_SIZE/2;
    if (mv->cam->Pos.X < -LAND_SIZE/2) mv->cam->Pos.X =  LAND_SIZE/2;
    if (mv->cam->Pos.Z >  LAND_SIZE/2) mv->cam->Pos.Z = -LAND_SIZE/2;
    if (mv->cam->Pos.Z < -LAND_SIZE/2) mv->cam->Pos.Z =  LAND_SIZE/2;
    if (mv->cam->Pos.Y <  0          ) mv->cam->Pos.Y =  8999;
    if (mv->cam->Pos.Y >  8999       ) mv->cam->Pos.Y =  0;
    return;
}

int main(int argc, char *argv[])
{
    int err = 0;
    int frames = 0;
    time_t start = 0;
    time_t stop = 0;

    Metaverse mv;
    EONx_Console *con;
    EON_Rend *rend;
    EON_Frame *frame;
    uint64_t ts = 0;

    memset(&mv, 0, sizeof(mv));

    mv.logger = CX_log_open_console(CX_LOG_MARK, stderr);

    opts_Defaults(&mv.opts);
    err = opts_Parse(&mv.opts, argc, argv);
    if (err) {
        return opts_ErrCode(err);
    }

    if (mv.opts.seed == 0) {
        mv.opts.seed = time(0);
    }

    CX_log_trace(mv.logger, CX_LOG_INFO, EXE,
                 "Frame Size: %ix%i",
                 mv.opts.width, mv.opts.height);
    CX_log_trace(mv.logger, CX_LOG_INFO, EXE,
                 "Generation Seed: %i",
                 mv.opts.seed);

    srand(mv.opts.seed);

    EONx_ConsoleStartup("Eon3D Land Generator", NULL);

    con = EONx_ConsoleCreate(mv.opts.width, mv.opts.height, 90);

    EONx_ConsoleBindEventKey(con, EONx_Ks, onkey_Screenshot, &mv);
    EONx_ConsoleBindEventKey(con, EONx_Kq, onkey_Quit, &mv);
    EONx_ConsoleBindEventKey(con, EONx_Kt, onkey_Toggle, &mv);
    EONx_ConsoleBindEventKey(con, EONx_Kh, onkey_Toggle, &mv);
    EONx_ConsoleBindEventKey(con, EONx_KUP, onkey_Rotate, &mv);
    EONx_ConsoleBindEventKey(con, EONx_KDOWN, onkey_Rotate, &mv);

    frame = EONx_ConsoleGetFrame(con);
    mv.cam = EONx_ConsoleGetCamera(con);
    rend = EON_RendCreate(mv.cam);
    mv.cam->Pos.Y = 800; // move the camera up from the ground

    world_setupLandscape(&mv.world, mv.logger); // create landscape

    ts = eon_gettime_ms();
    while (!EONx_ConsoleNextEvent(con)) {
        // save time when the frame began, to be used later.
        uint64_t elapsed = 0;
        frames++;

        adjustVerse(&mv);

        EONx_ConsoleClearFrame(con);

        // otherwise, render the sky (but not the second sky),
        // and the land, with a far clip plane
        mv.cam->ClipBack = 10000.0;
        EON_RenderBegin(rend);
        EON_RenderMesh(rend, mv.world.sky);
        EON_RenderMesh(rend, mv.world.land);
        EON_RenderEnd(rend, frame);

        elapsed = (eon_gettime_ms() - ts) / 1000000;
        if (mv.ship.showing_hud) {
            EON_TextPrintf(mv.cam, frame,
                          "%.3f FPS\n%.2f:%.2f:%.2f POS\nspeed=%.1f",
                          (frames/ (double) elapsed),
                          mv.cam->Pos.X, mv.cam->Pos.Y, mv.cam->Pos.Z,
                          mv.ship.speed);
        }
        EONx_ConsoleShowFrame(con);

    }

    CX_log_trace(mv.logger, CX_LOG_INFO, EXE,
                 "%i frames in %f seconds: %.3f FPS\n",
                 frames, (double)stop-(double)start,
                 (double)frames/((double)stop-(double)start));

    world_cleanLandscape(&mv.world);

    EONx_ConsoleShutdown();

    return CX_log_close(mv.logger);
}

