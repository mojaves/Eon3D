/**************************************************************************
 * eon3dx_console.h -- Eon3D eXtension and companion tools                *
 *                  -- Graphical console based on libSDL.                 *
 * (C) 2010-2014 Francesco Romani <fromani at gmail dot com>              *
 *                                                                        *
 * This software is provided 'as-is', without any express or implied      *
 * warranty.  In no event will the authors be held liable for any damages *
 * arising from the use of this software.                                 *
 *                                                                        *
 * Permission is granted to anyone to use this software for any purpose,  *
 * including commercial applications, and to alter it and redistribute it *
 * freely, subject to the following restrictions:                         *
 *                                                                        *
 * 1. The origin of this software must not be misrepresented; you must    *
 *    not claim that you wrote the original software. If you use this     *
 *    software in a product, an acknowledgment in the product             *
 *    documentation would be appreciated but is not required.             *
 * 2. Altered source versions must be plainly marked as such, and must    *
 *    not be misrepresented as being the original software.               *
 * 3. This notice may not be removed or altered from any source           *
 *    distribution.                                                       *
 *                                                                        *
 **************************************************************************/

#ifndef EON3DX_CONSOLE_H
#define EON3DX_CONSOLE_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "eon3d.h"

/* intentional ASCII overlap */
typedef enum {
    EONx_KBACKSPACE     = 8,
    EONx_KTAB           = 9,
    EONx_KCLEAR         = 12,
    EONx_KRETURN        = 13,
    EONx_KPAUSE         = 19,
    EONx_KESCAPE        = 27,
    EONx_KSPACE         = 32,
    EONx_KEXCLAIM       = 33,
    EONx_KQUOTEDBL      = 34,
    EONx_KHASH          = 35,
    EONx_KDOLLAR        = 36,
    EONx_KAMPERSAND     = 38,
    EONx_KQUOTE         = 39,
    EONx_KLEFTPAREN     = 40,
    EONx_KRIGHTPAREN    = 41,
    EONx_KASTERISK      = 42,
    EONx_KPLUS          = 43,
    EONx_KCOMMA         = 44,
    EONx_KMINUS         = 45,
    EONx_KPERIOD        = 46,
    EONx_KSLASH         = 47,
    EONx_K0             = 48,
    EONx_K1             = 49,
    EONx_K2             = 50,
    EONx_K3             = 51,
    EONx_K4             = 52,
    EONx_K5             = 53,
    EONx_K6             = 54,
    EONx_K7             = 55,
    EONx_K8             = 56,
    EONx_K9             = 57,
    EONx_KCOLON         = 58,
    EONx_KSEMICOLON     = 59,
    EONx_KLESS          = 60,
    EONx_KEQUALS        = 61,
    EONx_KGREATER       = 62,
    EONx_KQUESTION      = 63,
    EONx_KAT            = 64,
    /* skip uppercase letters */
    EONx_KLEFTBRACKET   = 91,
    EONx_KBACKSLASH     = 92,
    EONx_KRIGHTBRACKET  = 93,
    EONx_KCARET         = 94,
    EONx_KUNDERSCORE    = 95,
    EONx_KBACKQUOTE     = 96,
    EONx_Ka             = 97,
    EONx_Kb             = 98,
    EONx_Kc             = 99,
    EONx_Kd             = 100,
    EONx_Ke             = 101,
    EONx_Kf             = 102,
    EONx_Kg             = 103,
    EONx_Kh             = 104,
    EONx_Ki             = 105,
    EONx_Kj             = 106,
    EONx_Kk             = 107,
    EONx_Kl             = 108,
    EONx_Km             = 109,
    EONx_Kn             = 110,
    EONx_Ko             = 111,
    EONx_Kp             = 112,
    EONx_Kq             = 113,
    EONx_Kr             = 114,
    EONx_Ks             = 115,
    EONx_Kt             = 116,
    EONx_Ku             = 117,
    EONx_Kv             = 118,
    EONx_Kw             = 119,
    EONx_Kx             = 120,
    EONx_Ky             = 121,
    EONx_Kz             = 122,
    EONx_KDELETE        = 127,
    /* extra keys */
    EONx_KUP            = 273,
    EONx_KDOWN          = 274,
    EONx_KRIGHT         = 275,
    EONx_KLEFT          = 276,
    EONx_KINSERT        = 277,
    EONx_KHOME          = 278,
    EONx_KEND           = 279,
    EONx_KPAGEUP        = 280,
    EONx_KPAGEDOWN      = 281,
    /* function keys */
    EONx_KF1            = 282,
    EONx_KF2            = 283,
    EONx_KF3            = 284,
    EONx_KF4            = 285,
    EONx_KF5            = 286,
    EONx_KF6            = 287,
    EONx_KF7            = 288,
    EONx_KF8            = 289,
    EONx_KF9            = 290,
    EONx_KF10           = 291,
    EONx_KF11           = 292,
    EONx_KF12           = 293,
    EONx_KF13           = 294,
    EONx_KF14           = 295,
    EONx_KF15           = 296,
    /* special keys */
    EONx_KNUMLOCK       = 300,
    EONx_KCAPSLOCK      = 301,
    EONx_KSCROLLOCK     = 302,
    EONx_KRSHIFT        = 303,
    EONx_KLSHIFT        = 304,
    EONx_KRCTRL         = 305,
    EONx_KLCTRL         = 306,
    EONx_KRALT          = 307,
    EONx_KLALT          = 308,
    EONx_KRMETA         = 309,
    EONx_KLMETA         = 310,
    EONx_KLSUPER        = 311,
    EONx_KRSUPER        = 312,
    EONx_KMODE          = 313,
    EONx_KCOMPOSE       = 314
} EONx_KeyCode;


typedef struct EONx_Console EONx_Console;

/**
 * Key handler callback. Get called for each key pressed.
 */
typedef int (*EONx_KeyHandler)(EONx_KeyCode key, int status,
                               EONx_Console *con, void *userdata);

/**
 * initializes the console backend.
 * Any call to console functions before this call will produce
 * unexpected behaviour.
 * Should be called once before to use any other console functions.
 * Further calls are useless but harmless.
 * @param title title of the graphical window when in foreground.
 * @param icon title of the graphical window when iconized (WARNING!)
 * @return 0 on success, !0 on failure.
 */
int EONx_ConsoleStartup(const char *title, const char *icon);

/**
 * finalizes and releases the console backend.
 * Any call to console functions after this call will produce
 * unexpected behaviour.
 * Should be called once after use any other console functions.
 * Further calls are useless but harmless.
 * @return 0 on success, !0 on failure.
 */
int EONx_ConsoleShutdown(void);

/**
 * gets a new console instance.
 * Depending on the actual implementation, a console can have multiple
 * independent instance, multiple references to the same instance
 * (aka the singleton pattern) or exactly one instance.
 * @param sw screen width (pixels) of the console.
 * @param sh screen height (pixels) of the console.
 * @param fov field of vision (degrees) of the console
 *        (you usually want to use 90 here).
 * @return NULL if a new console instance cannot be made,
 *         a valid pointer otherwise
 */
EONx_Console *EONx_ConsoleCreate(EON_uInt sw, EON_uInt sh, EON_Float fov);

/**
 * releases a console instance obtained through EONx_ConsoleCreate.
 * @see EONx_ConsoleCreate
 * @param ctx a pointer to the console to be freed.
 * @return always NULL (and never fails.)
*/
void *EONx_ConsoleDelete(EONx_Console *ctx);

/**
 * obtains a pointer to a Camera instance from a console instance.
 * The returned pointer is a singleton.
 * There is no need to free() or destroy in any way the returned
 * pointer.
 * @param ctx a pointer to a console instance.
 * @return a pointer to a Camera bound to the given console instance.
 */
EON_Cam *EONx_ConsoleGetCamera(EONx_Console *ctx);

/**
 * binds an handler to the event generated by a key hit.
 * The handler will block the event loop and
 * will run in the same thread running EONx_ConsoleNextEvent
 * @param ctx console instance to bind the event/handler to.
 * @key value of the key to be handled. Can either be an ASCII
 *      code or a SDL keymap value (XXX)
 * @param handler event handling callback.
 * @userdata pointer to opaque data to be passed to the callback.
 * @return 0 on success, <0 on errors.
 */
int EONx_ConsoleBindEventKey(EONx_Console *ctx, EONx_KeyCode key,
                             EONx_KeyHandler handler, void *userdata);

/**
 * polls the console backend for the next event to be processed.
 * This function will NOT block the caller if no events are available.
 * If an event is available, calls all handlers registered for it,
 * or silently discards it if no handlers are available.
 * @param ctx pointer to the console instance to use.
 * @return if any handler has run, returns the value of the first handler
 *         registered which returned !0 (e.g. the error of ther first
 *         handler which run and failed).
 *         if no handler has run, returns 0 un success or !0 on error.
 */
int EONx_ConsoleNextEvent(EONx_Console *ctx);

/**
 * obtains a pointer to a Camera instance from a console instance.
 * The returned pointer is a singleton.
 * There is no need to free() or destroy in any way the returned
 * pointer.
 * @param ctx a pointer to a console instance.
 * @return a pointer to a Camera bound to the given console instance.
 */
EON_Frame *EONx_ConsoleGetFrame(EONx_Console *ctx);

/**
 * clears the current console frame.
 * @param ctx a pointer to a console instance.
 * @returns 0 un success, <0 on error.
 */
int EONx_ConsoleClearFrame(EONx_Console *ctx);

/**
 * displays the current frame on the console.
 * @param ctx a pointer to a console instance.
 * @return 0 un success, <0 on error.
 */
int EONx_ConsoleShowFrame(EONx_Console *ctx);

/* helpers */

/**
 * makes an unique yet human friendly name for a screenshot.
 * @param ctx a pointer to a console instance.
 * @param buf the buffer to be used to hold the name.
 * @param len length of the given buffer.
 * @param pfx prefix of the name to be built.
 * @param ext extension of the name to be built, if any.
 * @return a pointer to buf on success, NULL on error.
 */
const char *EONx_ConsoleMakeName(EONx_Console *ctx,
                                 char *buf, size_t len,
                                 const char *pfx, const char *ext);

/**
 * saves the current frame on a given file(path).
 * @param ctx a pointer to a console instance.
 * @param filename filename (path) of the file to be saved.
 * @param filetype hint of the file type to be saved.
 *        Case insensitive.
 * @return 0 un success, <0 on error.
 */
int EONx_ConsoleSaveFrame(EONx_Console *ctx,
                          const char *filename, const char *filetype);

/**
 * gets an human-readable description of the last error occurred.
 * There is no need to free() or destroy in any way the returned
 * pointer.
 * @param ctx a pointer to a console instance.
 * @return a pointer to the description of the last error occurred
 *         as string.
 */
const char *EONx_ConsoleGetError(EONx_Console *ctx);

#endif /* EON3DX_CONSOLE_H */

/* vim: set ts=4 sw=4 et */
/* EOF */

