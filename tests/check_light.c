/**************************************************************************
 * check_light: light manipulation unit tests                             *
 **************************************************************************/
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "config.h"

#include "eon3d.h" 


/*************************************************************************/

START_TEST(test_light_create_trivial)
{
    EON_Light *L = EON_LightCreate();
    ck_assert_ptr_ne(L, NULL);
    EON_LightDelete(L);
}
END_TEST

START_TEST(test_light_create_full1)
{
    EON_Light *L = EON_LightNew(EON_LIGHT_POINT,
                                0.0, 0.0, 0.0,
                                1.0, 1.0);
    ck_assert_ptr_ne(L, NULL);
    EON_LightDelete(L);
}
END_TEST

START_TEST(test_light_point_pos)
{
    EON_Double x = 1.0;
    EON_Double y = 2.0;
    EON_Double z = 3.0;
    EON_Light *L = EON_LightCreate();
    EON_LightSet(L, EON_LIGHT_POINT, x, y, z, 1.0, 1.0);
    ck_assert_int_eq(EON_DoubleEquals(L->Pos.X, x), 1);
    ck_assert_int_eq(EON_DoubleEquals(L->Pos.Y, y), 1);
    ck_assert_int_eq(EON_DoubleEquals(L->Pos.Z, z), 1);
    EON_LightDelete(L);
}
END_TEST

START_TEST(test_light_vector_type)
{
    EON_Double x = 1.0;
    EON_Double y = 2.0;
    EON_Double z = 3.0;
    EON_Light *L = EON_LightNew(EON_LIGHT_VECTOR, x, y, z, 1.0, 1.0);
    ck_assert_int_eq(L->Type, EON_LIGHT_VECTOR);
    EON_LightDelete(L);
}
END_TEST




/*************************************************************************/

TCase *eon3d_testCaseLight(void)
{
    TCase *tc = tcase_create("eon3d.core.light");
    tcase_add_test(tc, test_light_create_trivial);
    tcase_add_test(tc, test_light_create_full1);
    tcase_add_test(tc, test_light_point_pos);
    tcase_add_test(tc, test_light_vector_type);
    return tc;
}

static Suite *eon3d_suiteLight(void)
{
    TCase *tc = eon3d_testCaseLight();
    Suite *s = suite_create("eon3d.core.light");
    suite_add_tcase(s, tc);
    return s;
}

/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = eon3d_suiteLight();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

