/**************************************************************************
 * check_material: material unit tests                                          *
 **************************************************************************/
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "config.h"

#include "logkit.h"
#include "eon3d.h" 


/*************************************************************************/

START_TEST(test_material_create)
{
    EON_Mat *M = EON_MatCreate();
    ck_assert_ptr_ne(M, NULL);
    ck_assert_ptr_ne(M->_PutFace, NULL);
    EON_MatDelete(M);
}
END_TEST

START_TEST(test_material_info)
{
    CX_LogContext *sink = CX_log_open_null();
    EON_Mat *M = EON_MatCreate();
    EON_MatInfo(M, sink); // FIXME
    EON_MatDelete(M);
    CX_log_close(sink);
}
END_TEST

START_TEST(test_material_init_flat)
{
    EON_Mat *M = EON_MatCreate();
    void *orig = M->_PutFace;
    M->ShadeType = EON_SHADE_GOURAUD;
    EON_MatInit(M);
    ck_assert_ptr_ne(M->_PutFace, orig);
    EON_MatDelete(M);
}
END_TEST


/*************************************************************************/

TCase *eon3d_testCaseMaterial(void)
{
    TCase *tc = tcase_create("eon3d.core.material");
    tcase_add_test(tc, test_material_create);
    tcase_add_test(tc, test_material_info);
    tcase_add_test(tc, test_material_init_flat);
    return tc;
}

static Suite *eon3d_suiteMaterial(void)
{
    TCase *tc = eon3d_testCaseMaterial();
    Suite *s = suite_create("eon3d.core.material");
    suite_add_tcase(s, tc);
    return s;
}

/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = eon3d_suiteMaterial();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

