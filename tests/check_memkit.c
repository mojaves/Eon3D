/**************************************************************************
 * check_memkit: memory allocation routines unit test                     *
 **************************************************************************/
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "memorykit.h"


#define CHUNK_SMALL_LEN 128

/*************************************************************************/

START_TEST(test_CX_malloc_free)
{
    void *ptr = CX_malloc(CHUNK_SMALL_LEN);
    ck_assert(ptr != NULL);
    CX_free(ptr);
}
END_TEST

/*************************************************************************/

START_TEST(test_CX_zalloc_free)
{
    int ret = 0;
    uint8_t buf[CHUNK_SMALL_LEN];
    uint8_t *ptr = CX_zalloc(CHUNK_SMALL_LEN);
    ck_assert(ptr != NULL);
    memset(buf, 0, sizeof(buf));
    ret = memcmp(buf, ptr, CHUNK_SMALL_LEN);
    ck_assert_int_eq(ret, 0);
    CX_free(ptr);
}
END_TEST

/*************************************************************************/

START_TEST(test_CX_realloc_free)
{
    uint8_t *ptr = CX_realloc(NULL, CHUNK_SMALL_LEN);
    ck_assert(ptr != NULL);
    CX_free(ptr);
}
END_TEST

/*************************************************************************/

static Suite *CX_mem_suite(void)
{
  Suite *s = suite_create("eon3d.core.memkit");
  TCase *tc = tcase_create("eon3d.core.memkit");

  tcase_add_test(tc, test_CX_malloc_free);
  tcase_add_test(tc, test_CX_zalloc_free);
  tcase_add_test(tc, test_CX_realloc_free);
  suite_add_tcase(s, tc);

  return s;
}

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = CX_mem_suite();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

