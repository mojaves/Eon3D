/**************************************************************************
 * check_object: object manipulation unit test                            *
 **************************************************************************/
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "config.h"

#include "eon3d.h" 


/*************************************************************************/

START_TEST(test_object)
{
    /* TODO */
}
END_TEST


/*************************************************************************/

TCase *eon3d_testCaseObject(void)
{
    TCase *tcObject = tcase_create("eon3d.core.object");
    tcase_add_test(tcObject, test_object);

    return tcObject;
}

static Suite *eon3d_suiteObject(void)
{
    TCase *tc = eon3d_testCaseObject();
    Suite *s = suite_create("eon3d.core.object");
    suite_add_tcase(s, tc);
    return s;
}

/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = eon3d_suiteObject();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

