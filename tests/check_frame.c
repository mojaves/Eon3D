/**************************************************************************
 * check_frame: frame unit tests                                          *
 **************************************************************************/
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "config.h"

#include "eon3d.h" 


/*************************************************************************/

START_TEST(test_frame_create_trivial)
{
    EON_Frame *F = EON_FrameCreate(800, 600, 4);
    ck_assert_ptr_ne(F, NULL);
    EON_FrameDelete(F);
}
END_TEST

START_TEST(test_frame_create_properties)
{
    EON_Frame *F = EON_FrameCreate(800, 600, 4);
    ck_assert(F->Width == 800);
    ck_assert(F->Height == 600);
    ck_assert(F->Bpp == 4);
    ck_assert_ptr_ne(F->Data, NULL);
    ck_assert_ptr_ne(F->ZBuffer, NULL);
    EON_FrameDelete(F);
}
END_TEST

START_TEST(test_frame_create_zero_width)
{
    EON_Frame *F = EON_FrameCreate(0, 600, 4);
    ck_assert_ptr_eq(F, NULL);
}
END_TEST

START_TEST(test_frame_create_zero_height)
{
    EON_Frame *F = EON_FrameCreate(800, 0, 4);
    ck_assert_ptr_eq(F, NULL);
}
END_TEST

START_TEST(test_frame_create_zero_bpp)
{
    EON_Frame *F = EON_FrameCreate(800, 600, 0);
    ck_assert_ptr_eq(F, NULL);
}
END_TEST

START_TEST(test_frame_create_properties_after_clear)
{
    EON_Frame *F = EON_FrameCreate(800, 600, 4);
    EON_FrameClear(F);
    ck_assert(F->Width == 800);
    ck_assert(F->Height == 600);
    ck_assert(F->Bpp == 4);
    ck_assert_ptr_ne(F->Data, NULL);
    ck_assert_ptr_ne(F->ZBuffer, NULL);
    EON_FrameDelete(F);
}
END_TEST

START_TEST(test_frame_size)
{
    EON_Frame *F = EON_FrameCreate(800, 600, 4);
    EON_uInt32 SZ = EON_FrameSize(F);
    ck_assert(SZ >= 800 * 600 * 4);
    EON_FrameDelete(F);
}
END_TEST



/*************************************************************************/

TCase *eon3d_testCaseFrame(void)
{
    TCase *tc = tcase_create("eon3d.core.frame");
    tcase_add_test(tc, test_frame_create_trivial);
    tcase_add_test(tc, test_frame_create_zero_width);
    tcase_add_test(tc, test_frame_create_zero_height);
    tcase_add_test(tc, test_frame_create_zero_bpp);
    tcase_add_test(tc, test_frame_create_properties);
    tcase_add_test(tc, test_frame_create_properties_after_clear);
    tcase_add_test(tc, test_frame_size);
    return tc;
}

static Suite *eon3d_suiteFrame(void)
{
    TCase *tc = eon3d_testCaseFrame();
    Suite *s = suite_create("eon3d.core.frame");
    suite_add_tcase(s, tc);
    return s;
}

/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = eon3d_suiteFrame();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

