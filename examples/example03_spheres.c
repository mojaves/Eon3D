/* Rotates a flat shaded sphere */

#include <stdio.h>
#include <time.h>

#include <eon3d.h>
#include <eon3dx.h>
#include <eon3dx_console.h>


int main()
{
    int frames = 0;
    time_t start = 0, stop = 0;
    EON_Light *Light;
    EON_Mesh *Sphere;
    EON_Mat *SphereMat;
    EON_Cam *Camera;
    EON_Rend *Rend;
    EON_Frame *Frame;
    EONx_Console *Console;
    EON_Double radius = 50.0;
    EON_Double div = 512;

    EONx_ConsoleStartup("Eon3D :: example 1", NULL);

    SphereMat = EON_MatCreate();    // Create the material for the sphere
    SphereMat->ShadeType = EON_SHADE_GOURAUD; // Make the sphere flat shaded

    SphereMat->Ambient.R = 14;
    SphereMat->Ambient.G = 45;
    SphereMat->Ambient.B = 155;

    EON_MatInit(SphereMat);    // Initialize the material

    Console = EONx_ConsoleCreate(800, // Screen width
                                 600, // Screen height
                                 90.0 // Field of view
                                 );

    Sphere = EON_MakeSphere(radius, div, div, SphereMat);

    Frame = EONx_ConsoleGetFrame(Console);
    Camera = EONx_ConsoleGetCamera(Console);
    Camera->Pos.Z = -300; // Back the camera up from the origin

    Light = EON_LightNew(EON_LIGHT_VECTOR, // vector light
                            0.0,0.0,0.0, // rotation angles
                            1.0, // intensity
                            1.0); // falloff, not used for vector lights

    Rend = EON_RendCreate(Camera);

    start = time(NULL);
    while (!EONx_ConsoleNextEvent(Console)) { // While the keyboard hasn't been touched
        Sphere->Position.Y += 1.0; // Rotate by 1 degree
        EONx_ConsoleClearFrame(Console);
        EON_RenderBegin(Rend);           // Start rendering with the camera
        EON_RenderLight(Rend, Light); // Render our light
        EON_RenderMesh(Rend, Sphere);    // Render our object
        EON_RenderEnd(Rend, Frame);   // Finish rendering
        EONx_ConsoleShowFrame(Console);
        frames++;
    }
    stop = time(NULL);

    fprintf(stderr, "(%s) %i frames in %f seconds: %.3f FPS\n",
            __FILE__,
            frames, (double)stop-(double)start,
            (double)frames/((double)stop-(double)start));

    return EONx_ConsoleShutdown();
}

