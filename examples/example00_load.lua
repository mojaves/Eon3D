eon = require("leon3d")

if #arg ~= 2 then
	error("usage: " .. arg[0] .. " plymodel distance")
end

filename = arg[1]
distance = tonumber(arg[2])


eon.console.startup("Eon3D :: LUA example 0")

mat = eon.material.new()
mat.shadetype = 2 -- FIXME
mat.ambient.R = 200
mat.ambient.G = 200
mat.ambient.B = 200
mat:init()

model = eon.mesh.readply(filename, mat)

con = eon.console.new(800, 600, 90.0)
con.camera.position.Z = -distance

light = eon.light.new(1, -- FIXME
		      0.0, 0.0, 0.0, 
		      1.0,
		      1.0)
rend = eon.renderer.new(con.camera)

while con:nextevent() == 0 do
	model.rotation.X = model.rotation.X + 1.0
	model.rotation.Y = model.rotation.Y + 1.0
	model.rotation.Z = model.rotation.Z + 1.0

	con:clearframe()

	rend:renderbegin()
	rend:renderlight(light)
	rend:rendermesh(model)
	rend:renderend(con.frame)

	con:showframe()
end

-- atexit, really.
eon.console.shutdown()
