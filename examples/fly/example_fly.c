#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <sys/time.h>
#include <time.h>
#include <errno.h>

#include <eon3d.h>
#include <eon3dx.h>
#include <eon3dx_console.h>
#include <eon3dx_reader.h>

#include <logkit.h>

extern const unsigned long GROUND_PCX_SIZE;
extern const unsigned char GROUND_PCX_DATA[];

extern const unsigned long SKY_PCX_SIZE;
extern const unsigned char SKY_PCX_DATA[];

extern const unsigned long SKY2_PCX_SIZE;
extern const unsigned char SKY2_PCX_DATA[];


enum {
    LAND_SIZE = 65000, /* Physical size of land */
    LAND_DIV  = 128    /* Number of divisions of the land.
                          Higher number == more polygons */
};

EON_Texture *EONx_ReadPCXTexBuf(const uint8_t *buf, size_t len);

void setup_materials(EONx_Console *con, EON_Mat **mat,
                     CX_LogContext *Logger)
{
    // create our 3 materials, make the fourth null so that EON_MatMakeOptPal2()
    // knows where to stop
    mat[0] = EON_MatCreate();
    mat[1] = EON_MatCreate();
    mat[2] = EON_MatCreate();
    mat[3] = 0;

    // set up material 0 (the ground)
    mat[0]->ShadeType = EON_SHADE_GOURAUD_DISTANCE;
    mat[0]->Shininess = 1;
    mat[0]->Ambient.R = 255;
    mat[0]->Ambient.G = 255;
    mat[0]->Ambient.B = 255;
    mat[0]->Diffuse.R = 127;
    mat[0]->Diffuse.G = 127;
    mat[0]->Diffuse.B = 127;
    mat[0]->Specular.R = 127;
    mat[0]->Specular.G = 127;
    mat[0]->Specular.B = 127;
    mat[0]->FadeDist = 10000.0;
    mat[0]->Texture = EONx_ReadPCXTex("ground.pcx");
    if (mat[0]->Texture == NULL) {
        mat[0]->Texture = EONx_ReadPCXTexBuf(GROUND_PCX_DATA, GROUND_PCX_SIZE);
    }
    mat[0]->TexScaling = 40.0*LAND_SIZE/50000;
    mat[0]->PerspectiveCorrect = 16;
    EON_TexInfo(mat[0]->Texture, Logger);

    // set up material 1 (the sky)
    mat[1]->ShadeType = EON_SHADE_GOURAUD_DISTANCE;
    mat[1]->Shininess = 1;
    mat[1]->Ambient.R = 255;
    mat[1]->Ambient.G = 255;
    mat[1]->Ambient.B = 255;
    mat[1]->Diffuse.R = 127;
    mat[1]->Diffuse.G = 127;
    mat[1]->Diffuse.B = 127;
    mat[1]->Specular.R = 127;
    mat[1]->Specular.G = 127;
    mat[1]->Specular.B = 127;
    mat[1]->FadeDist = 10000.0;
    mat[1]->Texture = EONx_ReadPCXTex("sky.pcx");
    if (mat[1]->Texture == NULL) {
        mat[1]->Texture = EONx_ReadPCXTexBuf(SKY_PCX_DATA, SKY_PCX_SIZE);
    }
    mat[1]->TexScaling = 45.0*LAND_SIZE/50000;
    mat[1]->PerspectiveCorrect = 32;
    EON_TexInfo(mat[1]->Texture, Logger);

    // set up material 2 (the second sky)
    mat[2]->ShadeType = EON_SHADE_NONE;
    mat[2]->Shininess = 1;
    mat[2]->Texture = EONx_ReadPCXTex("sky2.pcx");
    if (mat[2]->Texture == NULL) {
        mat[2]->Texture = EONx_ReadPCXTexBuf(SKY2_PCX_DATA, SKY2_PCX_SIZE);
    }
    mat[2]->TexScaling = 10.0; //200.0*LAND_SIZE/50000;
    mat[2]->PerspectiveCorrect = 2;
    EON_TexInfo(mat[2]->Texture, Logger);

    // intialize the materials
    EON_MatInit(mat[0]);
    EON_MatInfo(mat[0], Logger);
    EON_MatInit(mat[1]);
    EON_MatInfo(mat[1], Logger);
    EON_MatInit(mat[2]);
    EON_MatInfo(mat[2], Logger);

    return;
}

typedef struct Landscape_ {
    EON_Mesh *land;
    EON_Mesh *sky;
    EON_Mesh *sky2;
} Landscape;

void setup_landscape(Landscape *ls, EON_Mat *m, EON_Mat *sm, EON_Mat *sm2)
{
    int i;
    // make our root object the land
    EON_Mesh *o = EON_MakePlane(LAND_SIZE,LAND_SIZE,LAND_DIV-1,m);
    // give it a nice random bumpy effect
    for (i = 0; i < o->NumVertices; i ++)
        o->Vertices[i].c.Y += (float) (rand()%1400)-700;
    // gotta recalculate normals for backface culling to work right
    EON_MeshCalcNormals(o);
    ls->land = o;

    // Make our first child the first sky
    ls->sky = EON_MakePlane(LAND_SIZE,LAND_SIZE,1,sm);
    ls->sky->Position.Y = 2000;
    ls->sky->BackfaceCull = 0;

    // and the second the second sky
    ls->sky2 = EON_MakeSphere(LAND_SIZE,10,10,sm2);
    ls->sky2->Position.Y = 2000;
    EON_MeshFlipNormals(ls->sky2);

    return;
}

// FIXME: move to/from SDL
uint64_t eon_gettime_ms(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec * 1000000 + tv.tv_usec);
}  

int main(int argc, char *argv[])
{
    int frames = 0;
    EON_Mat *mat[3];
    EON_Cam *cam;
    EON_Mesh *land;
    EON_Mesh *sky, *sky2;
    Landscape ls;
    EONx_Console *con;
    EON_Rend *rend;
    EON_Frame *frame;
    uint64_t ts = 0;

    CX_LogContext *Logger = CX_log_open_console(CX_LOG_MARK, stderr);

    srand(0); // initialize prng

    EONx_ConsoleStartup("Eon3D :: Fly v1.1", NULL);
    con = EONx_ConsoleCreate(800, // Screen width
                             600, // Screen height
                             90.0 // Field of view
                             );

    frame = EONx_ConsoleGetFrame(con);
    cam = EONx_ConsoleGetCamera(con);
    rend = EON_RendCreate(cam);
    cam->Pos.Y = 800; // move the camera up from the ground

    setup_materials(con, mat, Logger); // intialize materials and palette

    memset(&ls, 0, sizeof(ls));
    setup_landscape(&ls, mat[0],mat[1],mat[2]); // create landscape
    land = ls.land;
    sky = ls.sky;
    sky2 = ls.sky2;

    EON_MeshInfo(land, Logger);
    EON_MeshInfo(sky,  Logger);
    EON_MeshInfo(sky2, Logger);

    frames = 0;     // set up for framerate counter
    ts = eon_gettime_ms();
    while (!EONx_ConsoleNextEvent(con)) {
        // save time when the frame began, to be used later.
        uint64_t elapsed = 0;
        frames++;

        cam->Pos.Z += 1;

        EONx_ConsoleClearFrame(con);

        if (cam->Pos.Y > 2000) {
            // if above the sky, only render the skies, with no far clip plane
            cam->ClipBack = 0.0;
            EON_RenderBegin(rend);
            EON_RenderMesh(rend, sky);
            EON_RenderMesh(rend, sky2);
        } else {
            // otherwise, render the sky (but not the second sky),
            // and the land, with a far clip plane
            cam->ClipBack = 10000.0;
            EON_RenderBegin(rend);
            EON_RenderMesh(rend, sky);
            EON_RenderMesh(rend, land);
        }
        EON_RenderEnd(rend, frame);

        elapsed = (eon_gettime_ms() - ts) / 1000000;
        EON_TextPrintf(cam, frame,
                      "%.3f FPS",
                      (frames/ (double) elapsed));

        EONx_ConsoleShowFrame(con);

        // wraparound
        if (cam->Pos.X >  LAND_SIZE/2) cam->Pos.X = -LAND_SIZE/2;
        if (cam->Pos.X < -LAND_SIZE/2) cam->Pos.X =  LAND_SIZE/2;
        if (cam->Pos.Z >  LAND_SIZE/2) cam->Pos.Z = -LAND_SIZE/2;
        if (cam->Pos.Z < -LAND_SIZE/2) cam->Pos.Z =  LAND_SIZE/2;
        if (cam->Pos.Y <  0          ) cam->Pos.Y =  8999;
        if (cam->Pos.Y >  8999       ) cam->Pos.Y =  0;
    }

    EON_MeshDelete(land);
    EON_MeshDelete(sky);
    EON_MeshDelete(sky2);
    EON_MatDelete(mat[0]);
    EON_MatDelete(mat[1]);
    EON_MatDelete(mat[2]);

    EONx_ConsoleShutdown();

    return CX_log_close(Logger);
}

