eon = require("leon3d")

eon.console.startup("Eon3D :: LUA example 1")

mat = eon.material.new()
mat.shadetype = 2 -- FIXME
mat.ambient.R = 132
mat.ambient.G = 0
mat.ambient.B = 0
mat:init()

cube = eon.mesh.box(100, 100, 100, mat)

con = eon.console.new(800, 600, 90.0)
con.camera.position.Z = -300

light = eon.light.new(1, -- FIXME
		      0.0, 0.0, 0.0, 
		      1.0,
		      1.0)
rend = eon.renderer.new(con.camera)

while con:nextevent() == 0 do
	cube.rotation.X = cube.rotation.X + 1.0
	cube.rotation.Y = cube.rotation.Y + 1.0
	cube.rotation.Z = cube.rotation.Z + 1.0

	con:clearframe()

	rend:renderbegin()
	rend:renderlight(light)
	rend:rendermesh(cube)
	rend:renderend(con.frame)

	con:showframe()
end

-- atexit, really.
eon.console.shutdown()
